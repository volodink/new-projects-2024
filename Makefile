all: clean prepare
	cd presentations/new-projects-2024 && make && cp dist/presentation2024.pdf ../../dist/presentation2024.pdf

clean:
	rm -rf dist

prepare:
	mkdir -p dist

gitpod-bootstrap:
	sudo apt-get update && sudo apt-get dist-upgrade -y
