---
marp: true
theme: lection
paginate: true
---

<!-- _class: lead -->
<!-- _paginate: false -->

# Проекты на 2024-2025 год

---

<!-- _class: lead -->

# Константин Володин

# ст. преп. каф. ИТС



# <!--fit--> tg: @volodink


---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Unity

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Информационные технологии в дизайне

---

![bg h:100%](https://aip.com.ru/admin/files/global/journal/aip2020/sh_3.jpg)

---

![bg w:100%](https://wirenboard.com/wiki/images/thumb/5/5c/IntraSCADA.png/1280px-IntraSCADA.png)

---

# Семестровое задание: Интерфейсы в SCADA-системах

## Кого ищем и что делать
1. Дизайнер UI
- Понимает и может дизайн делать
- Изложить разработанный пример в виде туториала
2. Программист / DevOps
- Настроить IntraSCADA
- Изложить порядок установки и настройки в виде туториала

## Когда: до 4 семестра 

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Безопасность информационных систем

---

![bg](https://sun9-47.userapi.com/impg/xH8ScVi5jfvuieXCAdhT7PBJSGb6HdEmIcY-AQ/qSkS31GYSAg.jpg?size=1280x681&quality=96&sign=39a8c358b5c1762df622fcba08f40103&type=album)

---

![bg h:150%](https://bitmainrepair.ru/wp-content/uploads/2018/08/antminer-s9-kupit-v-moskve-1.jpg)

---

![bg h:100%](https://media.karousell.com/media/photos/products/2021/8/25/s9_antminer_control_board_1629881014_57542967_progressive.jpg)

---

# Семестровое задание: Брутфорс MD5 хешей

## Кого ищем и что делать 
1. Программист С/C++
- Разработка примера под FPGA Petalinux Linux 
- Изложить разработанный пример в виде туториала
2. Электронщик 
- Разбираться схемотехникой, FPGA (ПЛИС)
- Помогать программисту C/C++
- Изложить порядок установки и настройки в виде туториала

## Когда: сейчас, старт курса в сентябре

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Информационные системы и технологии

---

![bg h:100%](https://ae04.alicdn.com/kf/S2454689860b9461b90a51ebf8cc4199ec.jpg)

---

# Задачи

1. Дизайнеры запускают распределенный рендер в Blender
2. Безопасники запускают распределенный бутфорс хешей (md5, sha5)
3. Все запускают лабораторные работы которым нужны распределенные расчеты (Java, C/C++, TypeScript)
4. Все запускают лабораторные работы которым нужен сетевой стек (Java, C/C++, TypeScript)
...
остальное TBD

---

# Семестровое задание

## Тема: Разработка и запуск распределенных приложений 

## Кого ищем и что делать 
1. Программист
- Стек и обязанности: TBD
- Изложить порядок установки и настройки в виде туториала
2. DevOPS 
- Стек и обязанности: TBD
- Изложить порядок установки и настройки в виде туториала

## Когда: сейчас, старт курса в сентябре


---

<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!
# Вопросы?