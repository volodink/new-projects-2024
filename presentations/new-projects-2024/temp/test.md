---
marp: true
theme: lection
paginate: true

---
<style scoped>
    img[alt~="center"] {
      display: block;
      margin: 0 auto;
    }
</style>


# A Beautiful Headline

![w:500 center](https://csgeekshub.com/wp-content/uploads/2020/07/C-Program-compilation-steps-and-process.jpg)

---
<style scoped>
    section{
      justify-content: flex-start;
    }
</style>

# A Beautiful Headline

- some valid point
- another interesting fact
- even more convincing

![bg right:60% w:650](https://csgeekshub.com/wp-content/uploads/2020/07/C-Program-compilation-steps-and-process.jpg)

---
